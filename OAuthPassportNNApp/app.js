const express = require('express');
const hb = require('express-handlebars');
const authRoutes = require('./routes/auth-routes');
const passportSetup = require('./configs/passport-setup');

const app = express();

app.engine('handlebars', hb({defaultLayout: 'main'}));
app.set('view engine', 'handlebars');

//setup routes
app.use('/auth', authRoutes);

app.get('/', (req, res) => {
    res.render('home');
}) 

app.listen(3000, function() {
    console.log('app listing on port 3000');
});