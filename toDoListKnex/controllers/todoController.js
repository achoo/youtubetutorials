let bodyParser = require('body-parser');


let urlencodedParser = bodyParser.urlencoded({extended: false});


const knex = require('knex')({
    client: 'postgresql',
    connection: {
        database: 'todolist',
        user: 'postgres',
        password: 'postgres'
    }
});

module.exports = function(app) {

    app.get('/todo', function (req,res){

        let query = knex.select("text").from("lists");

        return query.then((rows) => {
            let data = rows
            res.render('todo', {todos: data});
        }).catch((error) => {
            console.log(error);
        })
    })

    app.post('/todo' , urlencodedParser, function(req, res) {

        let receivedData = req.body;
        
        let textData = {text: receivedData.item};

        let query = knex.insert(textData).into("lists");

        return query.then(() => {
            res.send('todo');
        }).catch((err) => {
            console.log(err);
        })
    })

    app.delete('/todo/:item', function(req, res) {

        let deleteValue = (req.params.item).replace(/-/g, ' ');

        let query = knex('lists').where({text: deleteValue}).select('id');

        query.then((rows)=>{
            let query2 = knex("lists").where("id", rows[0].id).del()
            query2.then(() => {
                res.send('todo');
            }).catch((err)=>{
            console.log(err)
        })
        }).catch((err)=>{
            console.log(err)
        })
    });
};

